package com.assetco.hotspots.optimization;

public enum ActivityLevel {
    Insignificant,
    Elevated,
    High,
    Extreme;

    boolean isLessThanOrEqualTo(ActivityLevel assetPerformance) {
        return compareTo(assetPerformance) <= 0;
    }
}
