package com.assetco.hotspots.optimization;

import org.junit.jupiter.api.*;

import static com.assetco.hotspots.optimization.ActivityLevel.*;
import static com.assetco.search.results.AssetVendorRelationshipLevel.*;
import static com.assetco.search.results.AssetVendorRelationshipLevel.Basic;
import static com.assetco.search.results.AssetVendorRelationshipLevel.Silver;

public class GoldDealsApplicatorTests extends DealsApplicatorTests {

    @BeforeEach
    public void setup() {
        baseSetup();
        setGoverningRelationshipLevel(Gold);
    }

    @Test
    public void basic() {
        givenAssetVendorLevel(Basic);

        whenApplyDeals();

        thenAssetIsInDeals(false);
    }

    @Test
    public void silverDealEligibleHighPerformanceHighVolume() {
        givenAssetVendorLevel(Silver);
        givenDealEligible(true);
        givenPerformance(High);
        givenVolume(High);

        whenApplyDeals();

        thenAssetIsInDeals(true);
    }

    @Test
    public void silverDealIneligibleHighPerformanceHighVolume() {
        givenAssetVendorLevel(Silver);
        givenDealEligible(false);
        givenPerformance(High);
        givenVolume(High);

        whenApplyDeals();

        thenAssetIsInDeals(false);
    }

    @Test
    public void silverDealEligibleElevatedPerformanceHighVolume() {
        givenAssetVendorLevel(Silver);
        givenDealEligible(true);
        givenPerformance(Elevated);
        givenVolume(High);

        whenApplyDeals();

        thenAssetIsInDeals(false);
    }

    @Test
    public void silverDealEligibleHighPerformanceElevatedVolume() {
        givenAssetVendorLevel(Silver);
        givenDealEligible(true);
        givenPerformance(High);
        givenVolume(Elevated);

        whenApplyDeals();

        thenAssetIsInDeals(false);
    }

    @Test
    public void goldDealEligibleElevatedPerformanceHighVolume() {
        givenAssetVendorLevel(Gold);
        givenDealEligible(true);
        givenPerformance(Elevated);
        givenVolume(High);

        whenApplyDeals();

        thenAssetIsInDeals(true);
    }

    @Test
    public void goldDealEligibleInsignificantPerformanceHighVolume() {
        givenAssetVendorLevel(Gold);
        givenDealEligible(true);
        givenPerformance(Insignificant);
        givenVolume(High);

        whenApplyDeals();

        thenAssetIsInDeals(false);
    }

    @Test
    public void goldDealIneligibleElevatedPerformanceElevatedVolume() {
        givenAssetVendorLevel(Gold);
        givenDealEligible(false);
        givenPerformance(Elevated);
        givenVolume(Elevated);

        whenApplyDeals();

        thenAssetIsInDeals(false);
    }

    @Test
    public void goldDealIneligibleHighPerformanceHighVolume() {
        givenAssetVendorLevel(Gold);
        givenDealEligible(false);
        givenPerformance(High);
        givenVolume(High);

        whenApplyDeals();

        thenAssetIsInDeals(true);
    }

    @Test
    public void goldDealEligibleHighPerformanceInsignificantVolume() {
        givenAssetVendorLevel(Gold);
        givenDealEligible(true);
        givenPerformance(High);
        givenVolume(Insignificant);

        whenApplyDeals();

        thenAssetIsInDeals(false);
    }
}
