package com.assetco.hotspots.optimization;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static com.assetco.hotspots.optimization.ActivityLevel.*;
import static com.assetco.search.results.AssetVendorRelationshipLevel.*;


class PartnerDealsApplicatorTests extends DealsApplicatorTests {

    @BeforeEach
    void setUp() {
        baseSetup();
        setGoverningRelationshipLevel(Partner);
    }

    @Test
    public void basic() {
        givenAssetVendorLevel(Basic);

        whenApplyDeals();

        thenAssetIsInDeals(false);
    }

    @Test
    public void partnerDealElevatedPerformance() {
        givenAssetVendorLevel(Partner);
        givenPerformance(Elevated);

        whenApplyDeals();

        thenAssetIsInDeals(true);
    }

    @Test
    public void partnerDealInsignificantPerformance() {
        givenAssetVendorLevel(Partner);
        givenPerformance(Insignificant);

        whenApplyDeals();

        thenAssetIsInDeals(false);
    }

    @Test
    public void goldDealIneligible() {
        givenAssetVendorLevel(Gold);
        givenDealEligible(false);

        whenApplyDeals();

        thenAssetIsInDeals(false);
    }

    @Test
    public void goldDealEligibleElevatedPerformance() {
        givenAssetVendorLevel(Gold);
        givenDealEligible(true);
        givenPerformance(Elevated);

        whenApplyDeals();

        thenAssetIsInDeals(false);
    }

    @Test
    public void goldDealEligibleHighPerformanceInsignificantVolume() {
        givenAssetVendorLevel(Gold);
        givenDealEligible(true);
        givenPerformance(High);
        givenVolume(Insignificant);

        whenApplyDeals();

        thenAssetIsInDeals(false);
    }

    @Test
    public void goldDealEligibleHighPerformanceElevatedVolume() {
        givenAssetVendorLevel(Gold);
        givenDealEligible(true);
        givenPerformance(High);
        givenVolume(Elevated);

        whenApplyDeals();

        thenAssetIsInDeals(true);
    }

    @Test
    public void silverDealIneligible() {
        givenAssetVendorLevel(Silver);
        givenDealEligible(false);

        whenApplyDeals();

        thenAssetIsInDeals(false);
    }

    @Test
    public void silverDealEligibleHighVolumeExtremePerformance() {
        givenAssetVendorLevel(Silver);
        givenDealEligible(true);
        givenVolume(High);
        givenPerformance(Extreme);

        whenApplyDeals();

        thenAssetIsInDeals(false);
    }

    @Test
    public void silverDealEligibleExtremeVolumeHighPerformance() {
        givenAssetVendorLevel(Silver);
        givenDealEligible(true);
        givenVolume(Extreme);
        givenPerformance(High);

        whenApplyDeals();

        thenAssetIsInDeals(false);
    }

    @Test
    public void silverDealEligibleExtremeVolumeExtremePerformance() {
        givenAssetVendorLevel(Silver);
        givenDealEligible(true);
        givenVolume(Extreme);
        givenPerformance(Extreme);

        whenApplyDeals();

        thenAssetIsInDeals(true);
    }

}