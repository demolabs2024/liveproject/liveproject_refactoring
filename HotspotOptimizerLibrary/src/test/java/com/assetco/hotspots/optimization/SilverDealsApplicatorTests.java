package com.assetco.hotspots.optimization;

import com.assetco.search.results.*;
import org.junit.jupiter.api.*;

import static com.assetco.hotspots.optimization.ActivityLevel.*;
import static com.assetco.search.results.AssetVendorRelationshipLevel.Basic;
import static com.assetco.search.results.AssetVendorRelationshipLevel.Silver;

public class SilverDealsApplicatorTests extends DealsApplicatorTests {
    @BeforeEach
    public void setup() {
        baseSetup();
        setGoverningRelationshipLevel(AssetVendorRelationshipLevel.Silver);
    }

    @Test
    public void basic() {
        givenAssetVendorLevel(Basic);

        whenApplyDeals();

        thenAssetIsInDeals(false);
    }

    @Test
    public void silverDealEligibleElevatedPerformanceHighVolume() {
        // By encapsulating the details in other methods which, themselves, are in other classes,
        // we allow each test to become a very clear statement of what the circumstances surrounding
        // the specified behavior are and what the expected outcome is.
        givenAssetVendorLevel(Silver);
        givenDealEligible(true);
        givenPerformance(Elevated);
        givenVolume(High);

        whenApplyDeals();

        thenAssetIsInDeals(true);
    }

    @Test
    public void silverDealEligibleInsignificantPerformanceHighVolume() {
        givenAssetVendorLevel(Silver);
        givenDealEligible(true);
        givenPerformance(Insignificant);
        givenVolume(High);

        whenApplyDeals();

        thenAssetIsInDeals(false);
    }

    @Test
    public void silverDealIneligibleElevatedPerformanceHighVolume() {
        givenAssetVendorLevel(Silver);
        givenDealEligible(false);
        givenPerformance(Elevated);
        givenVolume(High);

        whenApplyDeals();

        thenAssetIsInDeals(false);
    }

    @Test
    public void silverDealIneligibleHighPerformanceHighVolume() {
        givenAssetVendorLevel(Silver);
        givenDealEligible(false);
        givenPerformance(High);
        givenVolume(High);

        whenApplyDeals();

        thenAssetIsInDeals(true);
    }

    @Test
    public void silverDealEligibleHighPerformanceElevatedVolume() {
        givenAssetVendorLevel(Silver);
        givenDealEligible(true);
        givenPerformance(High);
        givenVolume(Elevated);

        whenApplyDeals();

        thenAssetIsInDeals(false);
    }
}
